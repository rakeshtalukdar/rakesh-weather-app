/* eslint-disable react/prop-types */
import React from "react";

const WeatherFactList = (props) => {
  return <li>{props.facts.fact}</li>;
};

export default WeatherFactList;
