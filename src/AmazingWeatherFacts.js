import React from "react";
import WeatherFactList from "./WeatherFactList";
import weatherFactsData from "./weatherFactsData";

const AmazingWeatherFacts = () => {
  const factListComponent = weatherFactsData.map((data) => {
    return <WeatherFactList key={data.id} facts={data} />;
  });
  return (
    <section className="amazing-weather-facts">
      <h1>10 Freaky weather facts</h1>
      <ol className="fact-lists">{factListComponent}</ol>
    </section>
  );
};

export default AmazingWeatherFacts;
