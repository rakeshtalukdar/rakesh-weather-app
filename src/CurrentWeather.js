/* eslint-disable react/prop-types */
import React from "react";
import WeatherForecastForFiveDays from "./WeatherForecastForFiveDays";

const CurrentWeather = (props) => {
  return (
    <main id="weatherInfo">
      <section className="currentWeather">
        <h1>Current Weather</h1>
        <h2 className="cityName">{props.city}</h2>
        <h2 className="currentTemperature">{props.temp}&deg;C</h2>
        <h4 className="temperatureFeelsLike">
          Feels Like {props.tempFeelsLike}&deg;C
        </h4>

        <p className="humidity">Humidity: {props.humidity}</p>

        <div className="iconAndWeatherDescription">
          <img
            className="weatherIcon"
            src={props.weatherIcon}
            alt={props.weatherDescription}
          />
          <p className="weatherDescription">{props.weatherDescription}</p>
        </div>
      </section>

      <WeatherForecastForFiveDays
        forecastForFiveDays={props.forecastForFiveDays}
      />
    </main>
  );
};

export default CurrentWeather;
