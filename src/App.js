import React from "react";
import Form from "./Form";
import CurrentWeather from "./CurrentWeather";
import AmazingWeatherFacts from "./AmazingWeatherFacts";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      city: "",
      temp: "",
      tempFeelsLike: "",
      tempMin: "",
      tempMax: "",
      humidity: "",
      weatherDescription: "",
      weatherIcon: "",
      iconId: "",
      forecastForFiveDays: [],
      isDataLoaded: false,
    };
    this.api = {
      key: process.env.REACT_APP_WEATHER_API_KEY,
      baseUrl: "https://api.openweathermap.org/data/2.5/",
    };
  }

  onClickCurrentLocation = (event) => {
    event.preventDefault();

    navigator.geolocation.getCurrentPosition((position) => {
      console.log("Inside onCLick");
      const longitude = position.coords.longitude;
      const latitude = position.coords.latitude;

      const currentWeatherUrl = `${this.api.baseUrl}weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${this.api.key}`;
      const forecastWeatherUrl = `${this.api.baseUrl}forecast?lat=${latitude}&lon=${longitude}&units=metric&appid=${this.api.key}`;

      this.getWeatherData(currentWeatherUrl);
      this.getWeatherData(forecastWeatherUrl);
    });
  };

  onSubmit = (event) => {
    event.preventDefault();
    const city = event.target.children.city.value;

    const currentWeatherUrl = `${this.api.baseUrl}weather?q=${city}&units=metric&appid=${this.api.key}`;
    const forecastWeatherUrl = `${this.api.baseUrl}forecast?q=${city}&units=metric&appid=${this.api.key}`;

    this.getWeatherData(currentWeatherUrl);
    this.getWeatherData(forecastWeatherUrl);
  };

  getWeatherData = async (url) => {
    try {
      const response = await fetch(url);
      const result = await response.json();
      this.setWeatherData(result);
    } catch (error) {
      console.error("Error occurred: ", error);
    }
  };

  setWeatherData = (data) => {
    if (data.cnt === undefined) {
      this.setState({
        city: data.name,
        temp: data.main.temp,
        tempMin: data.main.temp_min,
        tempMax: data.main.temp_max,
        tempFeelsLike: data.main.feels_like,
        humidity: data.main.humidity,
        weatherDescription: data.weather[0].description,
        weatherIcon: data.weather[0].icon,
        iconId: data.weather[0].id,
        isDataLoaded: true,
      });
    } else {
      const filteredData = data.list.filter((perDayData, index) => {
        return index % 8 === 0 ? perDayData : null;
      });

      this.setState({
        forecastForFiveDays: filteredData,
      });
    }
  };

  render() {
    return (
      <div className="App">
        <header id="header">
          <h1 className="heading">Weather</h1>
          <button id="currentLocationBtn" onClick={this.onClickCurrentLocation}>
            Current Location
          </button>
          <Form onSubmit={this.onSubmit} />
        </header>
        {this.state.isDataLoaded === true ? (
          <CurrentWeather
            city={this.state.city}
            tempMin={this.state.tempMin}
            tempMax={this.state.tempMax}
            temp={this.state.temp}
            tempFeelsLike={this.state.tempFeelsLike}
            humidity={this.state.humidity}
            weatherDescription={this.state.weatherDescription}
            weatherIcon={`https://openweathermap.org/img/wn/${this.state.weatherIcon}.png`}
            forecastForFiveDays={this.state.forecastForFiveDays}
          />
        ) : (
          <AmazingWeatherFacts />
        )}
      </div>
    );
  }
}

export default App;
