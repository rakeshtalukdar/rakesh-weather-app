/* eslint-disable react/prop-types */
import React from "react";
import DailyForecastsCards from "./DailyForecastsCards";

const WeatherForecastForFiveDays = (props) => {
  const dailyForecastCardComponent = props.forecastForFiveDays.map((data) => {
    return <DailyForecastsCards key={data.dt} data={data} />;
  });

  return (
    <section className="weatherForecastDaily">
      <h1 className="heading">Five Day Forecasts</h1>
      <div className="forecastDailyCards">{dailyForecastCardComponent}</div>
    </section>
  );
};

export default WeatherForecastForFiveDays;
