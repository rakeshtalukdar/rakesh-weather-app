/* eslint-disable react/prop-types */
import React from "react";

const Form = (props) => {
  return (
    <form id="search-form" onSubmit={props.onSubmit.onSubmit}>
      <input type="text" name="city" placeholder="Search City" />
      <button>Get Weather</button>
    </form>
  );
};

export default Form;
