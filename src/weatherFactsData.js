const weatherFacts = [
  {
    id: 1,
    fact: "You can tell the temperature by counting a cricket’s chirps!",
  },
  {
    id: 2,
    fact: "A heatwave can make train tracks bend!",
  },
  {
    id: 3,
    fact: " About 2,000 thunderstorms rain down on Earth every minute.",
  },
  {
    id: 4,
    fact:
      "Raindrops can be the size of a housefly and fall at more than 30kmph.",
  },
  {
    id: 5,
    fact: "Cape Farewell in Greenland is the windiest place on the planet.",
  },
  {
    id: 6,
    fact: "Hurricanes can push more than 6m of water ashore.",
  },
  {
    id: 7,
    fact: "In July 2001 the rainfall in Kerala, India, was blood red!",
  },
  {
    id: 8,
    fact: "A thunderstorm can produce 160kmph winds!",
  },
  {
    id: 9,
    fact:
      "In Antarctica, snow can fall so hard you can’t see your hand in front of your face.",
  },
  {
    id: 10,
    fact: "Some frogs get noisier just before it rains.",
  },
];

export default weatherFacts;
