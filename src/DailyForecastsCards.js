/* eslint-disable react/prop-types */
import React from "react";

const DailyForecastsCards = (props) => {
  const date = props.data.dt_txt.split(" ")[0];

  return (
    <div className="dailyCard" id={props.data.key}>
      <h3 className="dayAndDate">{date}</h3>
      <img
        className="forecastsWeatherIcon"
        src={`https://openweathermap.org/img/wn/${props.data.weather[0].icon}.png`}
        alt={props.data.weather[0].description}
      />
      <p className="temperatureMinMax">
        <span className="maxTemp">{props.data.main.temp_max}&deg;</span>
        <span className="minTemp">{props.data.main.temp_min}&deg;</span>
      </p>
      <p className="forecastWeatherDescription">
        {props.data.weather[0].description}
      </p>
    </div>
  );
};

export default DailyForecastsCards;
